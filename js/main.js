var ST = "#Stage_";
var S = "Stage_";
var NOTE_SCALE = 5;
var NOTE_QUESTION = 10;
ivo.info({
    title: "Actividad Design thinking y Lean StartUp, aliados para el trabajo creativo",
    autor: "Edilson Laverde Molina",
    date: "19-0-2022",
    email: "edilsonlaverde_182@hotmail.com",
    icon: 'https://www.ucundinamarca.edu.co/images/iconos/favicon_verde_1.png',
    facebook: "https://www.facebook.com/edilson.laverde.molina"
});
var audios = [
    {
        url: "sonidos/click.mp3",
        name: "clic"
    },{
        url: "sonidos/bien.mp3",
        name: "bien"
    },{
        url: "sonidos/mal.mp3",
        name: "mal"
    }
];

var correctos=0;
function main(sym) {
    var t = null;
    udec = ivo.structure({
        created: function () {
            t = this;
            t.animation();
            t.events();
            t.setText();
            //precarga audios//
            ivo.load_audio(audios, onComplete = function () {
                ivo(ST + "preload").hide();
                stage1.play();
            });
        },
        methods: {
            alertaShow: function (type,text) {
                ivo(ST + "indicador_fail").hide();
                ivo(ST + "indicador_good").hide();
                ivo(ST + "indicador_" + type).show();
                ivo(ST + "Text").html('<h1>Realimentación</h1>'+text);
                alerta.timeScale(1).play();
            },
            setText:function(){
                ivo(ST + "Text3").html(`
                    <h1 class="title">Descripción:</h1>
                    <p>La actividad tiene por objeto identificar los principales elementos de las metodologías innovadoras y sus diferencias.</p>
                    <h1 class="title">Instrucción:</h1>
                    <p>Tras la lectura de los casos de innovación y la lectura reconociendo Desging Thinking y Lean StartUp, relacione los conceptos analizados previamente con su correspondiente definición. Para ello, arrastre los términos de la izquierda al espacio que corresponda.</p> 
                `);
                ivo(ST+"texto").html(`
                <div class="creditos">
                    <p>
                        Metodologías de Innovación -  Design Thinking y Lean StarUp
                        <span>Design Thinking y Lean StarUp, aliados para el trabajo creativo</span>
                    </p>
                    <p>
                        Líder de Proyectos
                        <span>Viviana Jaramillo</span>
                    </p>
                    <p>
                        Experto en contenido
                        <span>Javier Hernando Ruiz Farfán</span>
                    </p>
                    <p>
                        Diseñador Instruccional	
                        <span>Amanda Pedraza</span>
                        <span>Gabriela Strauss</span>
                    </p>
                    <p>
                    Diseñador Multimedia	
                    <span>Ginés Velásquez	</span>
                    <span>Luis Francisco Sierra</span>
                </p>
                <p>
                        Diseño
                        <span>Andrés Martínez</span>
                        <span>Nazly María Victoria Díaz Vera</span>
                        <span>Sergio Sarmiento</span>
                    </p>
                    <p>
                        Programación
                        <span>Edilson Laverde Molina</span>
                    </p>
                    
                    <p>
                        Oficina de Educación Virtual y a Distancia<br>
                        Universidad de Cundinamarca<br>
                        2022
                    <p>
                </div>
                `);

            },
            game: function (){
                
                var moves=10;
                //declaramos los dragables del juego//
                $(".arrastrables").draggable({
                    revert: true,
                    start: function (event, ui) {
                        ivo.play("clic");
                    }
                });
                //declaramos los dropables del juego//
                $(".contenedores").droppable({
                    drop: function (event, ui) {
                        var id1 = ui.draggable.attr("id").split(S+"dra_")[1];
                        var id2 = $(this).attr("id").split(S+"dop_")[1];
                        ui.draggable.draggable("disable");
                        $(this).droppable("disable");
                        ui.draggable.position({
                            of: $(this),
                            my: "left top",
                            at: "left top"
                        });
                        ui.draggable.draggable("option", "revert", false);
                        ivo.play("clic");
                        console.log(id1+"=="+id2);
                        if (id1 == id2) {
                            correctos++;
                            ivo.play("bien");
                            moves--;
                        } else {
                            moves--;
                            ivo.play("mal");
                        }
                        if(moves==0){
                            //fin del juego//
                            //revisamos las escalas de aciertos//
                            //si esta entre 0 y 4 perdio//
                            if(correctos>=0 && correctos<=4){
                                t.alertaShow("fail","Animo, siga intentando. Recuerde revisar la lectura “Reconociendo Design Thinking y Lean StartUp”, de seguro será de utilidad para esta actividad.");
                            }
                            //si esta entre 5 y 9 gano//
                            if(correctos>=5 && correctos<=9){
                                t.alertaShow("good","Va por buen camino, pero es necesario analizar la lectura y cada uno de los elementos que allí se explican, para reconocerlos más fácilmente.");
                            }
                            //si es 10//
                            if(correctos==10){
                                t.alertaShow("good","Excelente, felicidades. Sin duda identifica a cabalidad los elementos de las dos metodologías de innovación Design Thinking y Lean StartUp.");
                            }
                            points = correctos / 2;
                            Scorm_mx = new MX_SCORM(false);
                            console.log("Nombre: " + Scorm_mx.info_user().name + " Id: " + Scorm_mx.info_user().id + " Nota: " + points);
                            Scorm_mx.set_score(points);
                        }

                    }
                }); //fin de dropable//

            },
            events: function () {
                var t = this;
                t.game();
                ivo(ST + "bts_start").on("click", function () {
                    ivo.play("clic");
                    stage1.reverse().timeScale(100);
                    stage2.play().timeScale(1);
                });
                ivo(ST + "next").on("click", function () {
                    ivo.play("clic");
                    stage2.reverse().timeScale(100);
                    stage3.play().timeScale(1);
                });
                ivo(ST + "btn_home").on("click", function () {
                    ivo.play("clic");
                    stage3.reverse().timeScale(100);
                    stage2.play().timeScale(1);
                });

                ivo(ST+"r_35").on("click", function () {
                    ivo.play("clic");
                    alerta.reverse().timeScale(100);
                })
                .on("mouseover", function () {

                })
                .on("mouseout", function () {
                    
                });
                //abrir creditos//
                ivo(ST + "btn_creditos").on("click", function () {
                    ivo.play("clic");
                    credits.timeScale(1).play();
                });

                //cerrar creditos//
                ivo(ST + "credits").on("click", function () {
                    ivo.play("clic");
                    credits.reverse().timeScale(100);
                });
            },
            animation: function () {
                stage1 = new TimelineMax();
                stage1.append(TweenMax.from(ST + "stage1", .8, {x: 1300, opacity: 0}), 0);
                stage1.append(TweenMax.from(ST + "resource_3", .8, {x: 1300, opacity: 0}), 0);
                stage1.append(TweenMax.from(ST + "resource_2", .8, {x: -4300, opacity: 0}), 0);
                stage1.append(TweenMax.from(ST + "bts_start", .8, {scale:0,x:200,y:200,rotation:1200, opacity: 0}), 0);
                stage1.stop();

                stage2 = new TimelineMax();
                stage2.append(TweenMax.from(ST + "stage2", .8, {x: 1300, opacity: 0}), 0);
                stage2.append(TweenMax.from(ST + "resource_6", .8, {x: -1300, opacity: 0}), 0);
                stage2.append(TweenMax.from(ST + "btn_creditos", .8, {x: 1300, opacity: 0}), 0);
                stage2.append(TweenMax.from(ST + "resource_4", .8, {x: 1300, opacity: 0}), 0);
                stage2.append(TweenMax.from(ST + "Text3", .8, { opacity: 0}), 0);
                stage2.append(TweenMax.from(ST + "next", .8, {scale:0,x:200,y:200,rotation:1200, opacity: 0}), 0);
                stage2.stop();

                stage3 = new TimelineMax();
                stage3.append(TweenMax.from(ST + "stage3", .8, {x: 1300, opacity: 0}), 0);
                stage3.append(TweenMax.from(ST + "resource_4-", .8, {x: 1300, opacity: 0}), 0);
                stage3.append(TweenMax.from(ST + "r_11", .8, {x: 1300, opacity: 0}), 0);
                stage3.append(TweenMax.from(ST + "r_10", .8, {x: 1300, opacity: 0}), 0);
                stage3.append(TweenMax.from(ST + "r_9", .8, {x: 1300, opacity: 0}), 0);
                stage3.append(TweenMax.staggerFrom(".arrastrables", .2, {x: 100, opacity: 0, scaleY: 6, ease: Elastic.easeOut.config(0.3, 0.4)}, .1), 0);
                stage3.append(TweenMax.from(ST + "btn_home", .8, {x: 1300, opacity: 0}), 0);
                stage3.stop();

                alerta = new TimelineMax();
                alerta.append(TweenMax.from(ST + "alert", .8, {x: 1300, opacity: 0}), 0);
                alerta.append(TweenMax.from(ST + "r_35", .8, {scale:0,x:200,y:200,rotation:1200, opacity: 0}), 0);
                alerta.stop();

                credits = new TimelineMax();
                credits.append(TweenMax.from(ST + "credits", .8, {x: 1300, opacity: 0}), 0);
                credits.append(TweenMax.from(ST + "label", .8, {x: 1300, opacity: 0}), 0);
                credits.append(TweenMax.from(ST + "footer", .8, {x: -4300, opacity: 0}), 0);
                credits.append(TweenMax.from(ST + "icon", .8, {x: 4300, opacity: 0}), 0);
                credits.stop();
            }
        }
    });
}