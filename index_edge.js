/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='images/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "both",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'stage1',
                            type: 'group',
                            rect: ['0', '0', '1306', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'fondo-1',
                                type: 'image',
                                rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"fondo-1.png",'0px','0px']
                            },
                            {
                                id: 'resource_3',
                                type: 'image',
                                rect: ['471px', '8px', '835px', '623px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"resource_3.png",'0px','0px']
                            },
                            {
                                id: 'resource_2',
                                type: 'image',
                                rect: ['97px', '119px', '465px', '302px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"resource_2.png",'0px','0px']
                            },
                            {
                                id: 'bts_start',
                                type: 'image',
                                rect: ['97px', '421px', '114px', '114px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"resource_1.png",'0px','0px'],
                                userClass: "pointer"
                            }]
                        },
                        {
                            id: 'stage2',
                            type: 'group',
                            rect: ['-2', '11', '1133', '812', 'auto', 'auto'],
                            c: [
                            {
                                id: 'resource_4',
                                type: 'image',
                                rect: ['587px', '427px', '546px', '385px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"resource_4.png",'0px','0px']
                            },
                            {
                                id: 'Text3',
                                type: 'text',
                                rect: ['34px', '99px', '722px', '458px', 'auto', 'auto'],
                                text: "<p style=\"margin: 0px;\">​</p>",
                                align: "left",
                                font: ['Arial, Helvetica, sans-serif', [24, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                                textStyle: ["", "", "", "", "none"]
                            },
                            {
                                id: 'resource_6',
                                type: 'image',
                                rect: ['0px', '25px', '651px', '51px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"resource_6.png",'0px','0px']
                            },
                            {
                                id: 'btn_creditos',
                                type: 'image',
                                rect: ['942px', '26px', '58px', '72px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"btn_creditos.png",'0px','0px'],
                                userClass: "pointer"
                            },
                            {
                                id: 'next',
                                type: 'image',
                                rect: ['57px', '508px', '46px', '46px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"next.svg",'0px','0px'],
                                userClass: "pointer"
                            }]
                        },
                        {
                            id: 'stage3',
                            type: 'group',
                            rect: ['101', '19', '1030', '804', 'auto', 'auto'],
                            c: [
                            {
                                id: 'resource_4-',
                                type: 'image',
                                rect: ['484px', '419px', '546px', '385px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"resource_4.png",'0px','0px']
                            },
                            {
                                id: 'r_10',
                                type: 'image',
                                rect: ['205px', '0px', '335px', '595px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"r_10.png",'0px','0px']
                            },
                            {
                                id: 'r_11',
                                type: 'image',
                                rect: ['0px', '1px', '178px', '594px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"r_11.png",'0px','0px']
                            },
                            {
                                id: 'r_9',
                                type: 'image',
                                rect: ['579px', '33px', '178px', '562px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"r_9.png",'0px','0px']
                            },
                            {
                                id: 'dra_10',
                                type: 'image',
                                rect: ['589px', '334px', '55px', '21px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"r_27.png",'0px','0px'],
                                userClass: "arrastrables"
                            },
                            {
                                id: 'dra_9',
                                type: 'image',
                                rect: ['589px', '446px', '91px', '20px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"r_29.png",'0px','0px'],
                                userClass: "arrastrables"
                            },
                            {
                                id: 'dra_8',
                                type: 'image',
                                rect: ['589px', '43px', '149px', '38px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"r_22.png",'0px','0px'],
                                userClass: "arrastrables"
                            },
                            {
                                id: 'dra_7',
                                type: 'image',
                                rect: ['589px', '390px', '127px', '21px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"r_28.png",'0px','0px'],
                                userClass: "arrastrables"
                            },
                            {
                                id: 'dra_6',
                                type: 'image',
                                rect: ['589px', '107px', '151px', '21px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"r_23.png",'0px','0px'],
                                userClass: "arrastrables"
                            },
                            {
                                id: 'dra_5',
                                type: 'image',
                                rect: ['589px', '555px', '73px', '21px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"r_31.png",'0px','0px'],
                                userClass: "arrastrables"
                            },
                            {
                                id: 'dra_4',
                                type: 'image',
                                rect: ['589px', '277px', '72px', '20px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"r_26.png",'0px','0px'],
                                userClass: "arrastrables"
                            },
                            {
                                id: 'dra_3',
                                type: 'image',
                                rect: ['589px', '222px', '85px', '20px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"r_25.png",'0px','0px'],
                                userClass: "arrastrables"
                            },
                            {
                                id: 'dra_2',
                                type: 'image',
                                rect: ['589px', '165px', '76px', '20px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"r_24.png",'0px','0px'],
                                userClass: "arrastrables"
                            },
                            {
                                id: 'dra_1',
                                type: 'image',
                                rect: ['589px', '500px', '25px', '20px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"r_30.png",'0px','0px'],
                                userClass: "arrastrables"
                            },
                            {
                                id: 'btn_home',
                                type: 'image',
                                rect: ['858px', '-11px', '58px', '73px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"r_32.png",'0px','0px'],
                                userClass: "pointer"
                            },
                            {
                                id: 'dop_1',
                                type: 'rect',
                                rect: ['0px', '36px', '178px', '55px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,0.00)"],
                                stroke: [1,"rgba(0,0,0,1)","none"],
                                userClass: "contenedores"
                            },
                            {
                                id: 'dop_2',
                                type: 'rect',
                                rect: ['0px', '91px', '178px', '55px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,0.00)"],
                                stroke: [1,"rgba(0,0,0,1)","none"],
                                userClass: "contenedores"
                            },
                            {
                                id: 'dop_3',
                                type: 'rect',
                                rect: ['0px', '147px', '178px', '55px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,0.00)"],
                                stroke: [1,"rgba(0,0,0,1)","none"],
                                userClass: "contenedores"
                            },
                            {
                                id: 'dop_4',
                                type: 'rect',
                                rect: ['0px', '203px', '178px', '55px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,0.00)"],
                                stroke: [1,"rgba(0,0,0,1)","none"],
                                userClass: "contenedores"
                            },
                            {
                                id: 'dop_5',
                                type: 'rect',
                                rect: ['0px', '259px', '178px', '55px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,0.00)"],
                                stroke: [1,"rgba(0,0,0,1)","none"],
                                userClass: "contenedores"
                            },
                            {
                                id: 'dop_6',
                                type: 'rect',
                                rect: ['0px', '315px', '178px', '55px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,0.00)"],
                                stroke: [1,"rgba(0,0,0,1)","none"],
                                userClass: "contenedores"
                            },
                            {
                                id: 'dop_7',
                                type: 'rect',
                                rect: ['0px', '371px', '178px', '55px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,0.00)"],
                                stroke: [1,"rgba(0,0,0,1)","none"],
                                userClass: "contenedores"
                            },
                            {
                                id: 'dop_8',
                                type: 'rect',
                                rect: ['0px', '427px', '178px', '55px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,0.00)"],
                                stroke: [1,"rgba(0,0,0,1)","none"],
                                userClass: "contenedores"
                            },
                            {
                                id: 'dop_9',
                                type: 'rect',
                                rect: ['0px', '483px', '178px', '55px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,0.00)"],
                                stroke: [1,"rgba(0,0,0,1)","none"],
                                userClass: "contenedores"
                            },
                            {
                                id: 'dop_10',
                                type: 'rect',
                                rect: ['0px', '539px', '178px', '55px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,0.00)"],
                                stroke: [1,"rgba(0,0,0,1)","none"],
                                userClass: "contenedores"
                            }]
                        },
                        {
                            id: 'credits',
                            type: 'group',
                            rect: ['-2', '11', '1133', '812', 'auto', 'auto'],
                            c: [
                            {
                                id: 'Rectangle',
                                type: 'rect',
                                rect: ['2px', '-11px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,1.00)"],
                                stroke: [0,"rgba(0,0,0,1)","none"]
                            },
                            {
                                id: 'footer',
                                type: 'image',
                                rect: ['587px', '427px', '546px', '385px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"resource_4.png",'0px','0px']
                            },
                            {
                                id: 'label',
                                type: 'image',
                                rect: ['0px', '25px', '651px', '51px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"resource_6.png",'0px','0px']
                            },
                            {
                                id: 'icon',
                                type: 'image',
                                rect: ['696px', '82px', '328px', '502px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"icon.svg",'0px','0px']
                            },
                            {
                                id: 'texto',
                                type: 'text',
                                rect: ['29px', '108px', '622px', '465px', 'auto', 'auto'],
                                text: "<p style=\"margin: 0px;\">​</p>",
                                font: ['Arial, Helvetica, sans-serif', [24, ""], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"]
                            }]
                        },
                        {
                            id: 'alert',
                            type: 'group',
                            rect: ['0', '0', '1024', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'fondo-2',
                                type: 'image',
                                rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                                opacity: '0.11382113821138',
                                fill: ["rgba(0,0,0,0)",im+"fondo-1.png",'0px','0px']
                            },
                            {
                                id: 'r_36',
                                type: 'image',
                                rect: ['116px', '156px', '793px', '354px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"r_36.png",'0px','0px']
                            },
                            {
                                id: 'Text',
                                type: 'text',
                                rect: ['163px', '160px', '437px', '197px', 'auto', 'auto'],
                                text: "<p style=\"margin: 0px;\">​</p>",
                                userClass: "alert",
                                font: ['Arial, Helvetica, sans-serif', [24, ""], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"]
                            },
                            {
                                id: 'indicador_good',
                                type: 'image',
                                rect: ['661px', '260px', '184px', '152px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"indicador_good.png",'0px','0px']
                            },
                            {
                                id: 'indicador_fail',
                                type: 'image',
                                rect: ['661px', '260px', '184px', '151px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"indicador_fail.png",'0px','0px']
                            },
                            {
                                id: 'r_35',
                                type: 'image',
                                rect: ['871px', '136px', '56px', '57px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"r_35.png",'0px','0px'],
                                userClass: "pointer"
                            }]
                        },
                        {
                            id: 'preload',
                            type: 'group',
                            rect: ['0', '0', '1024', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'fondo-preload',
                                type: 'rect',
                                rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,1)"],
                                stroke: [0,"rgb(0, 0, 0)","none"]
                            },
                            {
                                id: 'logo',
                                type: 'image',
                                rect: ['462px', '270px', '100px', '100px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"logo.svg",'0px','0px']
                            }]
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '1024px', '640px', 'auto', 'auto'],
                            overflow: 'hidden',
                            fill: ["rgba(255,255,255,1)"]
                        }
                    }
                },
                timeline: {
                    duration: 0,
                    autoPlay: true,
                    data: [

                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load("index_edgeActions.js");
})("EDGE-247297");
